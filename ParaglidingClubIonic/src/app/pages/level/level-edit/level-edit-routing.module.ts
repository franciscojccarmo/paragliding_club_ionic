import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LevelEditPage } from './level-edit.page';

const routes: Routes = [
  {
    path: '',
    component: LevelEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LevelEditPageRoutingModule {}
