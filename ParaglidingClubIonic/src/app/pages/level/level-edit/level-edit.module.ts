import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LevelEditPageRoutingModule } from './level-edit-routing.module';

import { LevelEditPage } from './level-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LevelEditPageRoutingModule
  ],
  declarations: [LevelEditPage]
})
export class LevelEditPageModule {}
