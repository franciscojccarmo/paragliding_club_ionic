import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LevelAddPageRoutingModule } from './level-add-routing.module';

import { LevelAddPage } from './level-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LevelAddPageRoutingModule
  ],
  declarations: [LevelAddPage]
})
export class LevelAddPageModule {}
