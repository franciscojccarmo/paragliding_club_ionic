import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LevelAddPage } from './level-add.page';

const routes: Routes = [
  {
    path: '',
    component: LevelAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LevelAddPageRoutingModule {}
