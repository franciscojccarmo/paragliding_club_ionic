import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SiteService } from 'src/app/shared/services/site.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Site } from 'src/app/shared/models/site';

@Component({
  selector: 'app-site-edit',
  templateUrl: './site-edit.page.html',
  styleUrls: ['./site-edit.page.scss'],
})
export class SiteEditPage implements OnInit {
  form: FormGroup;
  id: string;
  site: Site;
  route: ActivatedRoute;
  hidden: number;


  constructor(private fBuilder : FormBuilder, private siteService : SiteService, private navController: NavController, public _route: ActivatedRoute) { 
    this.route = _route;
  }

  ngOnInit() {
    this.form = this.fBuilder.group({
      name: new FormControl('', [Validators.required]),
      orientation: new FormControl('', [Validators.required]),
      altitudeTakeOff: new FormControl('', [Validators.required]),
      approachManeuver: new FormControl('', [Validators.required]),
      siteGeoCoordinate: new FormControl('', [Validators.required]),
      numberOfUse: new FormControl('', [Validators.required]),
      siteType: new FormControl('', [Validators.required]),
      levelId: new FormControl('', [Validators.required])
    })
  }

  ionViewDidEnter(){
    this.id = this.route.snapshot.params["id"];
    this.siteService.getSite(this.id).subscribe(data => {
      this.site = data;      
      this.hidden = this.site.siteType;
      this.form.patchValue(this.site)
    });
  }

  onSubmitAction(){
    if(this.hidden == 1){
      this.form.get("approachManeuver").setValue(" ");
    }else{
      this.form.get("altitudeTakeOff").setValue(0);
    };

    if(this.form.valid){
      this.siteService.editSite$(this.id, this.form.value).subscribe();
      this.navController.back();
    }
  }

}
