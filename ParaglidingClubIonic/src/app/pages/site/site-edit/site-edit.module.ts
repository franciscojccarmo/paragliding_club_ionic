import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiteEditPageRoutingModule } from './site-edit-routing.module';

import { SiteEditPage } from './site-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SiteEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SiteEditPage]
})
export class SiteEditPageModule {}
