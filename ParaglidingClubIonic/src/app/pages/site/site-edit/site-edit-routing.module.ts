import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteEditPage } from './site-edit.page';

const routes: Routes = [
  {
    path: ':id',
    component: SiteEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiteEditPageRoutingModule {}
