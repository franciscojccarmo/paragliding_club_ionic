import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiteListPageRoutingModule } from './site-list-routing.module';

import { SiteListPage } from './site-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SiteListPageRoutingModule
  ],
  declarations: [SiteListPage]
})
export class SiteListPageModule {}
