import { Component, OnInit } from '@angular/core';
import { SiteService } from 'src/app/shared/services/site.service';
import { Site } from 'src/app/shared/models/site';

@Component({
  selector: 'app-site-list',
  templateUrl: './site-list.page.html',
  styleUrls: ['./site-list.page.scss'],
})
export class SiteListPage implements OnInit {
  sites : Site[] = [];

  constructor(private siteService : SiteService) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.siteService.getSites().subscribe(data => this.sites = data);
  }

  deleteAction(id) {
    this.siteService.deleteSite(id).subscribe(() => {
      const index = this.sites.findIndex(s => s.id === id);
      this.sites.splice(index, 1);
      console.log(index);
    });
    console.log("Site deleted");
  }
}
