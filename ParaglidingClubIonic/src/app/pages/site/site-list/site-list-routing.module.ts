import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteListPage } from './site-list.page';

const routes: Routes = [
  {
    path: '',
    component: SiteListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiteListPageRoutingModule {}
