import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiteAddPageRoutingModule } from './site-add-routing.module';

import { SiteAddPage } from './site-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SiteAddPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SiteAddPage]
})
export class SiteAddPageModule {}
