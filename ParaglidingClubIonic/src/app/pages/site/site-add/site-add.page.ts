import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SiteService } from 'src/app/shared/services/site.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-site-add',
  templateUrl: './site-add.page.html',
  styleUrls: ['./site-add.page.scss'],
})
export class SiteAddPage implements OnInit {
  form : FormGroup;
  status : boolean = false;
  site : number;

  constructor(private fbuilder : FormBuilder, private siteService : SiteService, private navController : NavController) { }

  ngOnInit() {
    this.form = this.fbuilder.group({
      name: new FormControl('', [Validators.required]),
      orientation: new FormControl('', [Validators.required]),
      altitudeTakeOff: new FormControl('', [Validators.required]),
      approachManeuver: new FormControl('', [Validators.required]),
      siteGeoCoordinate: new FormControl('', [Validators.required]),
      numberOfUse: new FormControl('', [Validators.required]),
      siteType: new FormControl('', [Validators.required]),
      levelId: new FormControl('', [Validators.required])
    })
  }

  onSubmitAction(){
    if(this.form.valid){
      this.siteService.addSite(this.form.value).subscribe(data => console.log(data));
      this.navController.back();
    }
  }

  showForm(input: number){
    this.site = input;
    this.status = !this.status;
    this.form.get('siteType').setValue(input);
    this.form.get('siteType').disable();

    if(input == 1){
      this.form.get("approachManeuver").setValue(" ");
    }else{
      this.form.get("altitudeTakeOff").setValue(0);
    }
  }

}
