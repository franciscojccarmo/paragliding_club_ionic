import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteAddPage } from './site-add.page';

const routes: Routes = [
  {
    path: '',
    component: SiteAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiteAddPageRoutingModule {}
