import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PilotService } from 'src/app/shared/services/pilot.service';
import { NavController } from '@ionic/angular';
import { Pilot } from 'src/app/shared/models/pilot';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pilot-edit',
  templateUrl: './pilot-edit.page.html',
  styleUrls: ['./pilot-edit.page.scss'],
})
export class PilotEditPage implements OnInit {
  form: FormGroup;
  id : string;
  pilot: Pilot;
  route : ActivatedRoute;

  constructor(private fBuilder: FormBuilder, private pilotService: PilotService, private navController: NavController, public _route: ActivatedRoute ) {
    this.route = _route;
   }

  ngOnInit() {
    this.form = this.fBuilder.group({
      name: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      numberOfFlights: new FormControl('', [Validators.required])    
    });    
  }

  ionViewDidEnter(){
    this.id = this.route.snapshot.params["id"];
    this.pilotService.getPilot(this.id).subscribe(data => {this.pilot = data; this.form.patchValue(this.pilot); console.log(data)});
  }

  onSubmitAction(){
    this.form.get("numberOfFlights").setValue(this.pilot.numberOfFlights);
    if (this.form.valid){
      this.pilotService.editPilot$(this.id, this.form.value).subscribe(data => console.log(data));
      this.navController.back();
    }
  }

}
