import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotDetailPageRoutingModule } from './pilot-detail-routing.module';

import { PilotDetailPage } from './pilot-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotDetailPageRoutingModule
  ],
  declarations: [PilotDetailPage]
})
export class PilotDetailPageModule {}
