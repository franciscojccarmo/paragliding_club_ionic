import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PilotService } from 'src/app/shared/services/pilot.service';
import { NavController } from '@ionic/angular';
import { FlightService } from 'src/app/shared/services/flight.service';
import { Flight } from 'src/app/shared/models/flight';
import { Pilot } from 'src/app/shared/models/pilot';

@Component({
  selector: 'app-pilot-detail',
  templateUrl: './pilot-detail.page.html',
  styleUrls: ['./pilot-detail.page.scss'],
})
export class PilotDetailPage implements OnInit {
  id : string;
  pilot: Pilot;
  route : ActivatedRoute;
  flights : Flight[];
  numberOfFlights : number = 0;
  

  constructor(private flightService : FlightService, private pilotService: PilotService, private navController: NavController, public _route: ActivatedRoute ) { 
    this.route = _route;
  }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.id = this.route.snapshot.params['id'];
    this.pilotService.getPilot(this.id).subscribe(data => this.pilot = data);

    this.flightService.getFlights().subscribe(data => 
      {
        this.flights = data;
        this.numberOfFlights = this.flights.filter(flight => flight.pilotName == this.pilot.name).length;
      });
    
  }

}
