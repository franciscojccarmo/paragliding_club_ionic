import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotListPageRoutingModule } from './pilot-list-routing.module';

import { PilotListPage } from './pilot-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotListPageRoutingModule
  ],
  declarations: [PilotListPage]
})
export class PilotListPageModule {}
