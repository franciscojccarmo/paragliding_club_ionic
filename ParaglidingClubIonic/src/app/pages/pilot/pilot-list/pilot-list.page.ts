import { Component, OnInit } from '@angular/core';
import { Pilot } from '../../../shared/models/pilot';
import { PilotService } from '../../../shared/services/pilot.service';

@Component({
  selector: 'app-pilot-list',
  templateUrl: './pilot-list.page.html',
  styleUrls: ['./pilot-list.page.scss'],
})
export class PilotListPage implements OnInit {
  pilots: Pilot[] = [];

  constructor(private pilotService: PilotService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.pilotService.getPilots().subscribe(data => this.pilots = data);
    //this.pilotService.getPilots().subscribe(data => {
      //console.log(data);
      //data.forEach(pilot => this.pilots.push({...pilot, id: pilot.id}));
      // this.pilots = data;
    //} );
  }

  deleteAction(id) {
    this.pilotService.deletePilot(id).subscribe(() => {
      const index = this.pilots.findIndex(p => p.id === id);
      this.pilots.splice(index, 1);
      console.log(this.pilots);
    });
    console.log("Pilot deleted");
  }
}
