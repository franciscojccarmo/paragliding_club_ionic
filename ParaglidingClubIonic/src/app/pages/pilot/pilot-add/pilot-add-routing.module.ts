import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PilotAddPage } from './pilot-add.page';

const routes: Routes = [
  {
    path: '',
    component: PilotAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PilotAddPageRoutingModule {}
