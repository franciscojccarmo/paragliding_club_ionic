import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { PilotService } from 'src/app/shared/services/pilot.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-pilot-add',
  templateUrl: './pilot-add.page.html',
  styleUrls: ['./pilot-add.page.scss'],
})
export class PilotAddPage implements OnInit {
  form: FormGroup;

  constructor(private fBuilder: FormBuilder, private pilotService: PilotService, private navController: NavController) { }

  ngOnInit() {
    this.form = this.fBuilder.group({
      name: new FormControl('', [Validators.required]),      
      address: new FormControl('', [Validators.required]),
      numberOfFlights: new FormControl('', [Validators.required])     
    });
  }

  onSubmitAction(){
    if(this.form.valid){
      this.pilotService.addPilot(this.form.value).subscribe(data => console.log(data));
      this.navController.back();
    }
  }

}
