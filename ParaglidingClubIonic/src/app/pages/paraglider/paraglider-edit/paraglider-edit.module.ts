import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParagliderEditPageRoutingModule } from './paraglider-edit-routing.module';

import { ParagliderEditPage } from './paraglider-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParagliderEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ParagliderEditPage]
})
export class ParagliderEditPageModule {}
