import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Paraglider } from 'src/app/shared/models/paraglider';
import { ParagliderService } from 'src/app/shared/services/paraglider.service';

@Component({
  selector: 'app-paraglider-edit',
  templateUrl: './paraglider-edit.page.html',
  styleUrls: ['./paraglider-edit.page.scss'],
})
export class ParagliderEditPage implements OnInit {
  form: FormGroup;
  id : string;
  paraglider : Paraglider;
  route : ActivatedRoute;

  constructor(private fBuilder: FormBuilder, private paragliderService: ParagliderService, private navController: NavController, public _route: ActivatedRoute ) {
    this.route = _route;
   }

  ngOnInit() {
    this.form = this.fBuilder.group({
      name: new FormControl('', [Validators.required]),
      commissioningDate: new FormControl('', [Validators.required]),
      lastRevision: new FormControl('', Validators.required),
      paragliderModelApprovalNumber: new FormControl('', Validators.required),
      numberOfFlights: new FormControl('', [Validators.required])
    });    
  }

  ionViewDidEnter(){
    this.id = this.route.snapshot.params["id"];
    this.paragliderService.getParaglider(this.id).subscribe(data => {this.paraglider = data; this.form.patchValue(this.paraglider); console.log(data)});
  }

  onSubmitAction(){
    if (this.form.valid){
      this.paragliderService.editParaglider$(this.id, this.form.value).subscribe(data => console.log(data));
      this.navController.back();
    }
  }

}
