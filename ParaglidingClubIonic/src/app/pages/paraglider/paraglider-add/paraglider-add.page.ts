import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ParagliderService } from 'src/app/shared/services/paraglider.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-paraglider-add',
  templateUrl: './paraglider-add.page.html',
  styleUrls: ['./paraglider-add.page.scss'],
})
export class ParagliderAddPage implements OnInit {

  form: FormGroup;

  constructor(private fBuilder: FormBuilder, private paragliderService: ParagliderService, private navController: NavController) { }

  ngOnInit() {
    this.form = this.fBuilder.group({
      name: new FormControl('', [Validators.required]),      
      commissioningDate: new FormControl('', [Validators.required]),
      lastRevision: new FormControl('', [Validators.required]),
      paragliderModelApprovalNumber: new FormControl('', [Validators.required]),
      numberOfFlights: new FormControl('', [Validators.required])          
    });
  }

  onSubmitAction(){
    if(this.form.valid){
      this.paragliderService.addParaglider(this.form.value).subscribe(data => console.log(data));
      this.navController.back();
    }
  }

}
