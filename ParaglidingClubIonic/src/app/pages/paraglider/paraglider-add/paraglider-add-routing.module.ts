import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParagliderAddPage } from './paraglider-add.page';

const routes: Routes = [
  {
    path: '',
    component: ParagliderAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParagliderAddPageRoutingModule {}
