import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParagliderListPageRoutingModule } from './paraglider-list-routing.module';

import { ParagliderListPage } from './paraglider-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParagliderListPageRoutingModule
  ],
  declarations: [ParagliderListPage]
})
export class ParagliderListPageModule {}
