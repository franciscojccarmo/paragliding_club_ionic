import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParagliderListPage } from './paraglider-list.page';

const routes: Routes = [
  {
    path: '',
    component: ParagliderListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParagliderListPageRoutingModule {}
