import { Component, OnInit } from '@angular/core';
import { ParagliderService } from 'src/app/shared/services/paraglider.service';
import { Paraglider } from 'src/app/shared/models/paraglider';

@Component({
  selector: 'app-paraglider-list',
  templateUrl: './paraglider-list.page.html',
  styleUrls: ['./paraglider-list.page.scss'],
})
export class ParagliderListPage implements OnInit {
  paragliders: Paraglider[] = [];

  constructor(private paragliderService: ParagliderService) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.paragliderService.getParagliders().subscribe(data => this.paragliders = data);
    //this.paragliderService.getParagliders().subscribe(data => {
      //console.log(data);
      //data.forEach(paraglider => this.paragliders.push({...paraglider, id: paraglider.id }))
    //});
  }

  deleteAction(id){
    this.paragliderService.deleteParaglider(id).subscribe(() => {
      const index = this.paragliders.findIndex(pg => pg.id === id);
      this.paragliders.splice(index, 1);
    });
    console.log("Paraglider deleted");
  }
}
