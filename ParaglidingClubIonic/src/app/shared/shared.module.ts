import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PilotService } from './services/pilot.service';
import { ParagliderService } from './services/paraglider.service';
import { LevelService } from './services/level.service';
import { SiteService } from './services/site.service';
import { FlightService } from './services/flight.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [PilotService, ParagliderService, LevelService, SiteService, FlightService],
  
  exports: [HttpClientModule]
})
export class SharedModule { }
