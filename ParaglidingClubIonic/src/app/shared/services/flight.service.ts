import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Flight } from '../models/flight';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private httpClient: HttpClient) { }

  addFlight(flight: Flight): Observable<any> {
    return this.httpClient.post<Flight>('/api/Flights', flight).pipe(
      tap(_ => console.log('Added Flight')),
      catchError(this.handleError<Flight[]>('Add flight', []))
    );
  } 

  getFlight(id: string): Observable<Flight> {
    return this.httpClient.get<Flight>(`/api/flights/${id}`).pipe(
      tap(_ => console.log('Got a flight')),
      catchError(this.handleError<Flight>('Get one flight', null))
    );
  }

  getFlights(): Observable<Flight[]>{
    return this.httpClient.get<Flight[]>('/api/flights').pipe(
      tap(_ => console.log('Got a list of flights')),
      catchError(this.handleError<Flight[]>('Get all flights', []))
    );
  }

  deleteFlight(id): Observable<any> {
    return this.httpClient.delete(`/api/flights/${id}`).pipe(
      tap(_ => console.log('Flight deleted')),
        catchError(this.handleError<Flight>('Flight delete', null))
    );
  }

  editFlight$(id, flight: Flight): Observable<any> {
    return this.httpClient.put<any>(`/api/flights/${id}`, flight).pipe(
      tap(_ => console.log('Flight updated')),
      catchError(this.handleError<Flight>('Update Flight', null))
    );
  }

  private handleError<T>(operation= 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
