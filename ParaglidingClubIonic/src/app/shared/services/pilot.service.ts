import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pilot } from '../models/pilot';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PilotService {

  constructor(private httpClient: HttpClient) { }

  addPilot(pilot: Pilot): Observable<any> {
    return this.httpClient.post<Pilot>('/api/pilots', pilot).pipe(
      tap(_ => console.log('Added pilot')),
      catchError(this.handleError<Pilot[]>('Add pilot', []))
    );
  } 

  getPilot(id: string): Observable<Pilot> {
    return this.httpClient.get<Pilot>(`/api/pilots/${id}`).pipe(
      tap(_ => console.log('Got a pilot')),
      catchError(this.handleError<Pilot>('Get one pilot', null))
    );
  }

  getPilots(): Observable<Pilot[]>{
    return this.httpClient.get<Pilot[]>('/api/pilots').pipe(
      tap(_ => console.log('Got a list of pilots')),
      catchError(this.handleError<Pilot[]>('Get all pilots', []))
    );
  }

  deletePilot(id): Observable<any> {
    return this.httpClient.delete(`/api/pilots/${id}`).pipe(
      tap(_ => console.log('Pilot deleted')),
        catchError(this.handleError<Pilot>('Pilot delete', null))
    );
  }

  editPilot$(id, pilot: Pilot): Observable<any> {
    return this.httpClient.put<any>(`/api/pilots/${id}`, pilot).pipe(
      tap(_ => console.log('Pilot updated')),
      catchError(this.handleError<Pilot>('Update pilot', null))
    );
  }

  private handleError<T>(operation= 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
