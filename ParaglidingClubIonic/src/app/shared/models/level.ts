import { License } from './license';
import { Site } from './site';

export interface Level{
    id : number;
    name : string;
    skill : string;
    isActive : boolean;
    licenses : License[];
    sites : Site[];
}