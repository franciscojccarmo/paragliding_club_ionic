export interface Site{
    id : number;
    name : string;
    orientation : string;
    altitudeTakeOff : number;
    approachManeuver : string;
    siteGeoCoordinate : string;
    numberOfUse : number;
    siteType : number;
    levelId : number;
}