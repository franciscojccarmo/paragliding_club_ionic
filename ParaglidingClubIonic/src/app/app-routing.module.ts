import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'pilot-list',
    loadChildren: () => import('./pages/pilot/pilot-list/pilot-list.module').then( m => m.PilotListPageModule)
  },
  {
    path: 'pilot-add',
    loadChildren: () => import('./pages/pilot/pilot-add/pilot-add.module').then( m => m.PilotAddPageModule)
  },
  {
    path: 'pilot-edit',
    loadChildren: () => import('./pages/pilot/pilot-edit/pilot-edit.module').then( m => m.PilotEditPageModule)
  },
  {
    path: 'paraglider-list',
    loadChildren: () => import('./pages/paraglider/paraglider-list/paraglider-list.module').then( m => m.ParagliderListPageModule)
  },
  {
    path: 'paraglider-add',
    loadChildren: () => import('./pages/paraglider/paraglider-add/paraglider-add.module').then( m => m.ParagliderAddPageModule)
  },
  {
    path: 'paraglider-edit',
    loadChildren: () => import('./pages/paraglider/paraglider-edit/paraglider-edit.module').then( m => m.ParagliderEditPageModule)
  },
  {
    path: 'level-list',
    loadChildren: () => import('./pages/level/level-list/level-list.module').then( m => m.LevelListPageModule)
  },
  {
    path: 'level-add',
    loadChildren: () => import('./pages/level/level-add/level-add.module').then( m => m.LevelAddPageModule)
  },
  {
    path: 'level-edit',
    loadChildren: () => import('./pages/level/level-edit/level-edit.module').then( m => m.LevelEditPageModule)
  },
  {
    path: 'site-list',
    loadChildren: () => import('./pages/site/site-list/site-list.module').then( m => m.SiteListPageModule)
  },
  {
    path: 'site-add',
    loadChildren: () => import('./pages/site/site-add/site-add.module').then( m => m.SiteAddPageModule)
  },
  {
    path: 'site-edit',
    loadChildren: () => import('./pages/site/site-edit/site-edit.module').then( m => m.SiteEditPageModule)
  },
  {
    path: 'pilot-detail',
    loadChildren: () => import('./pages/pilot/pilot-detail/pilot-detail.module').then( m => m.PilotDetailPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
